# Installation
```npm install```

## Launch
```npm run dev```

# Deployment
## Export for Node server
```npm run build```

## Export to HTML
```npm run export```