import Error from './_error';
import fetch from 'isomorphic-unfetch';

import Layout from '../components/Layout';

const About = ({ user, statusCode }) => {
    if(statusCode) {
        return <Error statusCode={statusCode} />
    }

    return (
        <Layout title="About">
            <p>{user.name}</p>
            <p>Web developer</p>
            <img src="/2b.jpg" />
        </Layout>
    );
};

// export const getStaticProps = async () => {
//     const res = await fetch('https://jsonplaceholder.typicode.com/users/1');
//     const statusCode = res.status > 200 ? res.status : false;
//     const data = await res.json();
//     return {
//         props: {
//             user: data,
//             statusCode
//         }
//     };
// };

export const getServerSideProps = async (context) => {
    const id = context.query.id || 1;
    const res = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
    const data = await res.json();

    return {
        props: {
            user: data
        }
    };
};

export default About;