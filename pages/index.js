import Link from 'next/link';

import Layout from '../components/Layout';

const Index = () => (
    <Layout title="Home">
        <p>Welcome</p>
    </Layout>
);

export default Index;