import Layout from '../components/Layout';
import { useRouter } from 'next/router'

const Post = () => {
    const router = useRouter();

    return (
        <Layout title={router.query.title}>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Enim veniam tempora tenetur a architecto quae ipsum totam consequuntur ipsa amet dolorem eligendi, odio accusamus consequatur voluptate nisi incidunt, dolores consectetur.</p>
        </Layout>
    );
};

export default Post;